const http = require("http");
const websocket = require("ws");

const server = http.createServer((req, res) => {
  res.end("I am connected");
});
const wss = new websocket.Server({ server });

wss.on("headers", (headers, req) => {
  console.log(headers);
});

function toEvent(data) {
  try {
    const event = JSON.parse(data);
    return event;
  } catch (err) {
    return {};
  }
}

//Event: 'connection'
wss.on("connection", (ws, req) => {
  ws.send("This is a message from server, connection is established");
  //receive the message from client on Event: 'message'
  ws.on("message", function (data) {
    const event = toEvent(data);
    if (event.type) {
      this.emit(event.type, event.payload);
    } else {
      ws.send("Hello world!");
    }
  }).on("authenticate", (event) => {
    if (event.token === "admin") {
      ws.send("Correct credentials");
    } else {
      ws.send("Wrong credentials");
    }
  });
});

const PORT = 8000;

server.listen(PORT, () => {
  console.log(`Server is running at port: ${PORT}`);
});
